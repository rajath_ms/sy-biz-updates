'use strict';

// Define the sybuApp module
angular.module('bizNewsHist', [
  'ngRoute',
  'ngAnimate', 
  'core.news'
]);
