
angular.module('core.infra.fsoverlay')
.directive('syEscape', function () {
    return function (scope, element, attributes) {
        element.bind("keydown keyup", function (event) {
            if(event.which === 27) {
                scope.$apply(function (){
                    closeNav();
                });
                event.preventDefault();
            }
        });
    };
});

function openNav() {
    var element = document.getElementById("sy-profile-overlay");
    element.focus();
    element.style.height = "100%";
    element.style.width = "100%";
    element.style.top = "0";
    element.style.left = "0";
}

function closeNav() {
    var element = document.getElementById("sy-profile-overlay");
    element.style.height = "0%";
    element.style.width = "0%";
    element.style.top = "50%";
    element.style.left = "50%";
}
